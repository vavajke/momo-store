# Momo-store

Ссылки на публичные объекты проекта
- [Momo Store](http://pelmenster.ru)
- [Prometheus](https://prometheus.pelmenster.ru/)
- [Grafana](https://grafana.pelmenster.ru/)

Оглавление

- [Momo-store](#momo-store)
- [Короткое описание](#короткое-описание)
  - [Репозитории](#репозитории)
  - [Стек приложения](#стек-приложения)
  - [Стек инфраструктуры](#стек-инфраструктуры)
- [Приложение](#приложение)
  - [Frontend локальная сборка](#frontend-локальная-сборка)
  - [Backend локальная сборка](#backend-локальная-сборка)
  - [Сборка для деплоя](#сборка-для-деплоя)
  - [Версионирование](#версионирование)
  - [Gitflow](#gitflow)
  - [CI/CD](#cicd)
    - [Frontend](#frontend)
      - [build](#build)
      - [test](#test)
      - [release](#release)
    - [Backend](#backend)
      - [build](#build-1)
      - [test](#test-1)
      - [release](#release-1)
    - [Deploy](#deploy)
- [Инфраструктура](#инфраструктура)
  - [Terraform](#terraform)
  - [Helm приложения](#helm-приложения)
    - [Ingress](#ingress)
    - [Deploy](#deploy-1)
      - [values.yaml](#valuesyaml)
  - [Helm мониторинга](#helm-мониторинга)
  - [Loki](#loki)
  - [Prometheus](#prometheus)
  - [Grafana](#grafana)
    - [Dashboards](#dashboards)
      - [Примеры дашбордов](#примеры-дашбордов)
    - [Alerts](#alerts)
    - [Loki](#loki-1)
   
# Короткое описание

## Репозитории
- [Репозиторий приложения](https://gitlab.com/vavajke/momo-store)
- [Репозиторий инфраструктуры](https://gitlab.com/vavajke/momo-infrastructure)

## Стек приложения
- `Vue`
- `Go`
- `Docker`
- `Gitlab`

## Стек инфраструктуры
- `K8s`
- `Terraform`
- `Helm`
- `Kube-state-metrics`
- `Promtail`
- `Loki`
- `Prometheus`
- `Grafana`

# Приложение
Приложение состоит из frontend приложения на `Vue` и backend приложения на `Go`. Общение друг с другом осуществлено с помощью `REST API`. Стандартный порты 80 для frontend и 8081 для backend. 

[Репозиторий приложения](https://gitlab.com/vavajke/momo-store).

## Frontend локальная сборка
```bash
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

## Backend локальная сборка
```bash
go run ./cmd/api #Запуск приложения
go test -v ./... #Запуск тестов
```

## Сборка для деплоя
Для деплоя приложения созданы `Dockerfile`, из которых создаются образы. 

Frontend необходимо предварительно собрать, билд должен быть в каталоге `html`.

Backend собирается с помощью `Docker`, в образе остаётся только билд, необходимый для запуска приложения.

## Версионирование
Версии приложения создаются от числового идентификатора пайплайна. Последняя версия образов всегда тегируется `latest` тегом.

## Gitflow
Для реализации новых функций необходимо соблюдать gitflow. Новые ветки необходимо называть с префиксом `f-`. Далее ветки необходимо мержить в `master` ветку и удалять, но только при успешном прохождении pipeline стадии `test`.

## CI/CD
Процесс `CI/CD` реализован с помощью `Gitlab`. Включает в себя отдельные пайплайны для frontend и backend приложений и стадию деплоя в production окружение.

### Frontend
В пайплайне реализованы следующие стадии:

#### build
На этом этапе происходит сборка приложения из исходников для дальнешей запаковки и подготовки приложения к стадии проверки.

#### test
Стадия тестов. Для данного проекта были выбраны `sast` тестов из шаблона `Security/SAST.gitlab-ci.yml`. Это статические тесты безопасности приложения. Итоги прохождения тестов можно увидеть в артефакте `gl-sast-report.json`.

#### release
Этап сборки `Docker` образа и отправка образа в `Gitlab Container Registry` в соответствии с правилами версионирования.

### Backend
В пайплайне реализованы следующие стадии:

#### build
Этап необходим для проверки возможности сборки приложения. Если приложение с новым функционалом не проходит этап сборки, то оно требует доработки и фикса багов.

#### test
Стадия тестов. Для тестирования используются внутренние тесты `go test -v ./...` на работоспособность критического функционала и на соответствие отдаваемого контента. А так же используется сканирование кодовой базы на уязвимости с помощью `sonar-scanner`. Результаты сканирования доступны в [SonarQube](https://sonarqube.praktikum-services.ru/dashboard?id=maysak_momo).

#### release
Этап сборки `Docker` образа и отправка образа в `Gitlab Container Registry` в соответствии с правилами версионирования. В Docerfile реализована сборка и подготовка контейнера для запуска backend приложения на `alpine`.

### Deploy
Стадия деплоя проекта в production окружения. Запускается только для ветки `master`. Источник deploy пайплана находится в репозитории инфраструктуры, в репозитории приложения вызывается только триггер его запуска. Описание этого пайплайна можно найти в описании репозитория инфраструктуры (или ниже в этом документе).

# Инфраструктура
Инфраструктура состоит из множества инструментов, соответствует IaS подходу, стабильно работает на диапазоне до 500 rps (протестировано с помощью Yandex.Tank). 

Стек:
- `K8s`
- `Terraform`
- `Helm`
- `Kube-state-metrics`
- `Promtail`
- `Loki`
- `Prometheus`
- `Grafana`

[Репозиторий инфраструктуры](https://gitlab.com/vavajke/momo-infrastructure)


## Terraform
Инфраструктура для приложения описана в файле `main.tf` для использования с помощью Terraform. Состояние Terraform хранится в `s3`.
Кластер состоит из трёх самых простых нод.
```
# YAML
platform_id = "standard-v1"
resources {
    memory = 2
    cores  = 2
}
boot_disk {
    type = "network-hdd"
    size = 64
}
```

## Helm приложения
Для разворачивания приложения в k8s используется helm чарт, в котором описано всё, что необходимо для работы frontend и backend части приложения. Пакет можно посмотреть по [ссылке](https://nexus.praktikum-services.ru/#browse/browse:momo-store-maysak:momo-store).

Репозиторий: https://nexus.praktikum-services.ru/repository/momo-store-maysak/

Пакет: `momo-store-chart`

Для удобства релиза чарта в репозиторий создан bash-скрипт `helm.upload.sh`. Для его работы требуется `.env` файл.

### Ingress
Для правильного роутинга применен Ingress с правилами:

- `/api/`: ведет на backend с правилом rewrite на корень
- `/`: все остальные запросы идут на frontend

### Deploy
Деплой приложения описан в `.gitlab-ci.yml` с единственным шагом деплоя. Для успешного деалоя нужно не забыть добавить необходимы переменные в CI/CD Variables gitlab. Переменные `K8S_CONFIG`, `VALUES_YAML` и `NEXUS_PASSWORD` должны быть заданы в base64 формате.

Для доступа из пайпалайна к кластеру нужно предусмотреть наличие соответствующего `ServiceAccount`. Создать такой и получить токен к нему можно с помощью следующего набора команд:
```
# BASH
kubectl create serviceaccount cicd-user
kubectl create clusterrolebinding cicd-user-binding --clusterrole=cluster-admin --serviceaccount=default:cicd-user
kubectl get secret cicd-user-token-qtzfb -o=jsonpath="{.data.token}"
```

Также необходимо предусмотреть доступ `Deploy Job` к `Container Registry`. Для этого необходимо создать токен и создать dockerconfigjson. После этого использовать строку конфига, закодированную в base64, в файле `values.yaml` для чарта.

#### values.yaml
Ниже приведен пример `values.yaml` файла необходимого для работы с `helm` чартом приложения.
```
# YAML
frontend:
  image:
    repository: "registry.gitlab.com/vavajke/momo-store/frontend"
    tag: latest
  replicas: 1
  fqdn: "pelmenster.ru"
  port: 80
  backendName: backend
  backendPort: 8081
  admin_email: "vavajke@gmail.com"

backend:
  image:
    repository: "registry.gitlab.com/vavajke/momo-store/backend"
    tag: latest
  replicas: 1
  port: 8081
  vpa:
    enabled: false

global:
  environment: production
  dockerconfigjson: ewogICAgImF1dGhzIjogewogICAgIdCAgICJnaXRsYWIucHJha3Rpa3VtLXNlcnZpY2VzLnJ1OjUwNTAiOnsKICAgICAgICAgICAgInV123123uYW1lIjoia3ViZXJuZXRlcyIsCiAgICAgICAgICAgICJwYXNzasd29yZCI6InZ3321XhyM0ZXUWRtS3NGIiwKICAgICAgICAgICAgImF1dGgiOiJhM1ZpWlhKdVpYUmxjenAyZDFodmNFMU5SSGw0Y2pOR1YxRmtiVXR6Umc9PSIKICAgIAl9CiAgICB9Cn0=

```

Деплой запускается только на `master` ветке.

## Helm мониторинга

В качестве мониторинга приложения используется набор инструментов:
- `kube-state-metrics`
- `Promtail`
- `Loki`
- `Prometheus`
- `Grafana`

## Loki
Приёмник логов. Доступен по урлу [loki.pelmenster.ru](https://loki.pelmenster.ru/). Готов для забора логов.

## Prometheus
Сборщик метрик. Доступен по урлу [prometheus.pelmenster.ru](https://prometheus.pelmenster.ru/). 

Сконфигурирован на сборку метрик следующих ресурсов:
- `endpoints`
- `node`
- `pod`
- `cadvisor`

## Grafana
Инструмент мониторинга и алертинга. Доступен по урлу [grafana.pelmenster.ru](https://grafana.pelmenster.ru/).

Доступ:
- Логин: admin
- Пароль: h2Nsoc20KrX1LfeokMN9GZrMjhXwGVS01KniT4cl

Связан с Loki и Prometheus.

### Dashboards

Стандартные дашборды уже присутствует в системе и позволяет наблюдать за ресурсами системы `CPU, Memory, IO, Filesysyem`, за сетевой активностью `NGINX Ingress controller` и за метриками приложения `Momo-store metrics`.

#### Примеры дашбордов
>При клике на изображение можно перейти на полный скриншот

[![CPU, Memory, IO, Filesysyem](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/resources.mini.png)](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/resources.png)

Срез по ресурсам содержит в себе текущие показатели потребления памяти, cpu, диска и график их изменения. Показатели можно отфильтровать по ноде.

[Ссылка на дашборд](https://grafana.pelmenster.ru/d/JABGX_-mz/cpu-memory-io-filesysyem?orgId=1)

[![NGINX Ingress controller](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/ingress.mini.png)](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/ingress.png)

Так как вся сетевая активность настроена через `Nginx Ingress controller` было принято решение следить за его показателями. На дашборде можно наблюдать за текущими показателями операций запросов в секунду, активными соединениями, процентом успешных ответов, распределение нагрузки запросов по приложениям, использование контроллером памяти и cpu, среднюю скорость ответов на запросы.
Самый полезный фильтр это фильтр по приложению, но также доступны и другие фильтры, которые могут быть полезны при масштабировании.

[Ссылка на дашборд](https://grafana.pelmenster.ru/d/nginx/nginx-ingress-controller?orgId=1&refresh=5s)

[![Momo-store metrics](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/momo-store.mini.png)](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/momo-store.png)

Дашборд по метрикам из `backend`. Количество запросов к api в rps, скорость ответов системы по методам и количество заказов на сайте.

[Ссылка на дашборд](https://grafana.pelmenster.ru/d/TGyruEjnk/momo-store-dashboard?orgId=1)

### Alerts

Система обогащена алертами на различные критические события:
- Истощение ресурсов памяти, cpu, файловой системы
- Коды ошибок backend 500

Алертинг настроен на канал связи Telegram.

[![Momo-store metrics](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/alerts.mini.png)](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/alerts.png)

### Loki

В sources Grafana добален Loki, который собирает множество логов. В режиме Browse можно работать с логами с помощью LogQL.

[![Grafana-Loki](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/loki.mini.png)](https://storage.yandexcloud.net/momo-static/%D0%B4%D0%B8%D0%BF%D0%BB%D0%BE%D0%BC/loki.png)

# Momo Store aka Пельменная №2

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

[Momo Store](http://pelmenster.ru)

Стек приложения:
- `Vue`
- `Go`

## Frontend локальная сборка

```bash
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

## Backend локальная сборка

```bash
go run ./cmd/api
go test -v ./... 
```

## Сборка для деплоя
Для деплоя приложения созданы `Dockerfile`, из которых создаются образы. 

Frontend необходимо предварительно собрать, билд должен быть в каталоге `html`.

Backend собирается с помощью `Docker`, в образе остаётся только билд, необходимый для запуска приложения.

---

## Версионирование

Версии приложения создаются от числового идентификатора пайплайна. Последняя версия образов всегда тегируется `latest` тегом.

---

## Gitflow

Для реализации новых функций необходимо соблюдать gitflow. Новые ветки необходимо называть с префиксом `f-`. Далее ветки необходимо мержить в `master` ветку и удалять, но только при успешном прохождении pipeline стадии `test`.

---

## Инфраструктура

Инфраструктуры приложения реализована и описана в репозитории [momo-infrastructure](https://gitlab.com/vavajke/momo-infrastructure). 

Ознакомиться с ней можно в файле [README.md](https://gitlab.com/vavajke/momo-infrastructure/-/blob/master/README.md).